// Import required modules
const express = require('express');
const bodyParser = require('body-parser');

// Create an Express application
const app = express();

// Middleware to parse form data
app.use(bodyParser.urlencoded({ extended: true }));

// Serve static files from the public directory
app.use(express.static('public'));

// Route handler for serving the login page
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/login.html');
});

// Route handler for handling login form submissions
app.post('/login', (req, res) => {
  const { username, password } = req.body;

  // Check if username and password are correct (in this example, we hardcode a username and password)
  if (username === 'admin' && password === 'password') {
    res.send('Login successful!');
  } else {
    res.send('Invalid username or password.');
  }
});

// Start the server and listen on port 3000
app.listen(3000, () => {
  console.log('Server is running on http://localhost:3000');
});
